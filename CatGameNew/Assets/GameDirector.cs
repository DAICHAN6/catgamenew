﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GameDirector : MonoBehaviour
{
    GameObject LifeGauge;

    // Start is called before the first frame update
    void Start()
    {
        this.LifeGauge = GameObject.Find("LifeGauge");
    }

    // Update is called once per frame
    public void DecreaseHp()
    {
        this.LifeGauge.GetComponent<Image>().fillAmount -= 0.1f;
    }
}
